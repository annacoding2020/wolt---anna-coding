import { combineReducers } from 'redux';
import openingHour from './openingHourReduer';

export const initialState = {

};

export const rootReducer = combineReducers({
    openingHour,
});

export default rootReducer;

export type AppState = ReturnType<typeof rootReducer>

