import {
	SET_OPENING_HOUR,
	IS_OPENING_HOUR_SAVING,
	OpeningHourState,
	OpeningHourActionType,
} from '@actionTypes/OpeningHourTypes';

const initialOpeningHourState: OpeningHourState = {
	isSaving: false,
	data: {
		monday: [],
		tuesday: [
			{ type: 'open', value: 36000 },
			{ type: 'close', value: 64800 },
			{ type: 'close', value: 42800 },
			{ type: 'open', value: 48000 },
		],
		wednesday: [],
		thursday: [
			{ type: 'open', value: 36000 },
			{ type: 'close', value: 64800 },
		],
		friday: [{ type: 'open', value: 36000 }],
		saturday: [
			{ type: 'close', value: 3600 },
			{ type: 'open', value: 36000 },
		],
		sunday: [
			{ type: 'close', value: 3600 },
			{ type: 'open', value: 43200 },
			{ type: 'close', value: 75600 },
		],
	},
};

export default function articleReducer(state = initialOpeningHourState, action: OpeningHourActionType) {
	switch (action.type) {
		case IS_OPENING_HOUR_SAVING: {
			return {
				...state,
				isSaving: action.isSaving,
			};
		}
		case SET_OPENING_HOUR: {
			return {
				...state,
				data: action.payload,
			};
		}
		default:
			return state;
	}
}
