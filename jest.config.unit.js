const config  = require("./jest.config");

module.exports = {
    ...config,
    testRegex: "(.*\\.unit\\.(test|spec))\\.[jt]sx?$",
    "moduleNameMapper": {
        // "pages(.*)$": "<rootDir>/src/pages",
        "^.+\\.(css|less|scss)$": "identity-obj-proxy"
    },
};