
module.exports = {
    testPathIgnorePatterns: ['.next/', 'node_modules/'],
    moduleDirectories: [
        'node_modules',
        __dirname,
    ],
    moduleNameMapper: {
        "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m  4a|aac|oga)$": "<rootDir>/__mocks__/fileMock.js",
        "\\.(css|less|scss)$": "<rootDir>/__mocks__/styleMock.js"
    },
};