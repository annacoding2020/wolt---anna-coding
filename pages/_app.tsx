import { NextPageContext } from 'next';
import App, { AppContext, AppInitialProps } from 'next/app';
import Head from 'next/head';
import React from 'react';
import { Provider } from 'react-redux';
import withRedux from 'next-redux-wrapper';
import withReduxSaga from 'next-redux-saga';
import createStore, { SagaStore } from '@store/index';

import './_app.scss'

interface WoltContext extends NextPageContext {
	store: SagaStore;
}

class WoltApp extends App<WoltContext> {
	static async getInitialProps({ Component, ctx }: AppContext): Promise<AppInitialProps> {
		let pageProps = {};

		if (Component.getInitialProps) {
			pageProps = await Component.getInitialProps(ctx);
		}
		return { pageProps };
	}

	render() {
		const { Component, pageProps, store } = this.props;
		const languageCode = 'en';
		return (
			<Provider store={store}>
				<Head>
					<title>Wolt Coding Task</title>
          <meta name="language" key="language" content={languageCode} />
					<meta name="Description" key="Description" content="Wolt opening hours assignment (frontend)" />
					<meta name="keywords" key="keywords" content="Wolt, Opening hours" />
				</Head>
				<Component {...pageProps} />
			</Provider>
		);
	}
}

export default withRedux(createStore)(withReduxSaga(WoltApp));
