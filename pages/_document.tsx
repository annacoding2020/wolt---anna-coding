import Document, { Html, Head, Main, NextScript } from 'next/document';

class MyDocument extends Document<any> {
	static async getInitialProps(ctx) {
		const originalRenderPage = ctx.renderPage;
		ctx.renderPage = () =>
			originalRenderPage({
				// useful for wrapping the whole react tree
				enhanceApp: App => App,
				// useful for wrapping in a per-page basis
				enhanceComponent: Component => Component,
			});

		// Run the parent `getInitialProps` using `ctx` that now includes our custom `renderPage`
		const initialProps = await Document.getInitialProps(ctx);
		const languageTag = initialProps.head.find(
			(headItem: any) => headItem.type === 'meta' && headItem.props.name === 'language'
		);
		return { ...initialProps, languageCode: languageTag ? languageTag.props.content : 'fi' };
	}

	render() {
		return (
			<Html lang="en">
				<Head>
					<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700&display=swap" rel="stylesheet" />
					<meta key="compatible" httpEquiv="X-UA-Compatible" content="IE=edge" />
				</Head>
				<body>
					<Main />
					<NextScript />
				</body>
			</Html>
		);
	}
}

export default MyDocument;
