import React, { useState, useEffect, useRef } from 'react';
import { NextPage, NextPageContext } from 'next';
import { useDispatch, connect } from 'react-redux';
import { AppState } from '@reducers/index';
import dynamic from 'next/dynamic';

import MainLayout from '@components/MainLayout';
import OpeningHour from '@components/OpeningHour';
import { OpeningHourState, OpeningHourItem } from '@actionTypes/OpeningHourTypes';

const OpeningHourEditorWithNoSSR = dynamic(() => import('@components/OpeningHourEditor'), { ssr: false });

import './index.scss';
import { postOpeningHour } from '@actions/OpeningHour';

interface HomeProps {
	openingHour: OpeningHourState;
}

const Home: NextPage<HomeProps, {}> = ({ openingHour }) => {
	const dispatch = useDispatch();

	const handleOnSave = (data: OpeningHourItem[]): void => {
		dispatch(postOpeningHour(data));
	};

	return (
		<main>
			<MainLayout siderChildren={<OpeningHourEditorWithNoSSR onSave={handleOnSave} openingHour={openingHour} />}>
				<div className="home__page__box">
					<OpeningHour openingHour={openingHour} />
				</div>
			</MainLayout>
		</main>
	);
};

interface Context extends NextPageContext {
	store: any;
}

Home.getInitialProps = async ({}: Context) => {
	return {};
};

export default connect(({ openingHour }: AppState) => {
	return {
		openingHour,
	};
})(Home);
