import { all } from 'redux-saga/effects';

import OpeningHourSaga from './OpeningHour/OpeningHourSaga';

export default function* rootSaga() {
  yield all([
    ...OpeningHourSaga,
  ]);
}
