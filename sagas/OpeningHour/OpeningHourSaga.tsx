import { takeLatest, put } from 'redux-saga/effects';
import { SagaIterator } from '@redux-saga/core';
import * as Actions from '@actions/OpeningHour';
import * as ActionTypes from '@actionTypes/OpeningHourTypes';

export function* postOpeningHourReq({ payload }: ActionTypes.PostOpeningHour): SagaIterator {
	yield put(Actions.isOpeningHourSaving(true));
	try {
		// Should send to server here
		yield put(Actions.setOpeningHour({ ...payload }));
	} catch (error) {
		// Handle error
		yield put(Actions.setOpeningHour({}));
	}
}


const productsSagas = [
	takeLatest(ActionTypes.POST_OPENING_HOUR, postOpeningHourReq),
];

export default productsSagas;
