import { OpeningHourItem } from "@actionTypes/OpeningHourTypes";

export function compareOpeningHourItem(a: OpeningHourItem, b: OpeningHourItem):number {
    let comparison:number = 0;
    if (a.value > b.value) {
      comparison = 1;
    } else if (a.value < b.value) {
      comparison = -1;
    }
    return comparison;
  }
  