import { applyMiddleware, createStore, Store as ReduxStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootReducer, { initialState, AppState } from '@reducers/index';
import rootSaga from '@sagas/index';

const bindMiddleware = middleware => {
	if (process.env.NODE_ENV !== 'production') {
		const { composeWithDevTools } = require('redux-devtools-extension');
		var createLogger = require('redux-logger').createLogger;
		return composeWithDevTools(applyMiddleware(...middleware, createLogger()));
	}
	return applyMiddleware(...middleware);
};

type Store = ReduxStore<AppState>;

export interface SagaStore extends Store {
	sagaTask: any;
}

function configureStore(state = initialState): SagaStore {
	const sagaMiddleware = createSagaMiddleware();
	const store: SagaStore = createStore(rootReducer, state, bindMiddleware([sagaMiddleware]));

	store.sagaTask = sagaMiddleware.run(rootSaga);

	return store;
}

export default configureStore;
