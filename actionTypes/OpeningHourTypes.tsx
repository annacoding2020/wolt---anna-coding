import OpeningHourEditor from "@components/OpeningHourEditor";

export const POST_OPENING_HOUR = 'POST_OPENING_HOUR';
export const SET_OPENING_HOUR = 'SET_OPENING_HOUR';
export const IS_OPENING_HOUR_SAVING = 'IS_OPENING_HOUR_SAVING';

export type OpeningHourItem =  {
	type: 'close' | 'open',
	value: number
}

enum DayEnum {
    monday = 1,
    tuesday = 2,
    wednesday = 3,
    thursday = 4,
    friday = 5,
    saturday = 6,
    sunday = 7,
}

export type OpeningHour = {
	[key in keyof typeof DayEnum]: OpeningHourItem[] | []
} | {}

export interface PostOpeningHour{
	type: typeof POST_OPENING_HOUR;
	payload: OpeningHour;
}

export interface IsOpeningHourSaving {
	type: typeof IS_OPENING_HOUR_SAVING;
	isSaving:  boolean;
}

export interface SetOpeningHour{
	type: typeof SET_OPENING_HOUR;
	payload:  OpeningHour;
}

export type OpeningHourActionType = PostOpeningHour | SetOpeningHour | IsOpeningHourSaving;

export interface OpeningHourState {
	isSaving: boolean,
	data: OpeningHour
}