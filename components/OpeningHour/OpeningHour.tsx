import React, { useState, useEffect } from 'react';
import AceEditor from 'react-ace';
import { connect, useDispatch } from 'react-redux';
import { AppState } from '@reducers/index';
import { OpeningHourState, OpeningHourItem } from '@actionTypes/OpeningHourTypes';
import { Card, Icon } from 'antd';
import { compareOpeningHourItem } from '@modules/utils';
import moment from 'moment';
import 'moment-duration-format';

import './OpeningHour.scss';

interface OpeningHourProps {
	openingHour: OpeningHourState;
}

const OpeningHour: React.FunctionComponent<OpeningHourProps> = ({ openingHour }) => {
	const [openingHourData, setOpeningHourData] = useState<any[]>([]);

	useEffect(() => {
		const dataEntry: any[] = Object.entries({ ...openingHour.data });
		const openingHourData = [...dataEntry].map(([key, value], index) => {
			let _value = [...value.slice(0).sort(compareOpeningHourItem)];
			if (_value.length) {
				if (_value[0].type === 'close') {
					delete _value[0];
				}
				if (_value[_value.length - 1].type === 'open') {
					const nextValue = [
						...(index === dataEntry.length - 1 ? dataEntry[0][1] : dataEntry[index + 1][1]),
					].sort(compareOpeningHourItem);
					if (nextValue[0] && nextValue[0].type === 'close') {
						_value.push({ ...nextValue[0] });
					}
				}
			}
			return [key, _value];
		});
		setOpeningHourData(openingHourData);
	}, [openingHour]);

	const diplayOpeningHours = (ohs: OpeningHourItem[] | [], index: number): string => {
		if (!ohs.length) {
			return 'Closed';
		}
		const openingHours = (ohs || []).map(({ type, value }) => {
			const time = moment.utc(moment.duration(value, 'seconds').asMilliseconds()).format('h:mm A');
			if (type === 'close') {
				return ` - ${time}, `.replace(/:00/g, '');
			}
			return time.replace(/:00/g, '');
		});
		return openingHours.join('').replace(/,\s*$/, '');
	};

	const checkToday = (day: string): string => {
		if (
			moment()
				.format('dddd')
				.toLocaleLowerCase() === day
		) {
			return 'TODAY';
		}
	};
	return (
		<Card
			className="opening__hours__card"
			title={
				<>
					<Icon type="clock-circle" />
					Opening hours
				</>
			}
			bordered={false}
			style={{ width: 400 }}
		>
			<>
				{openingHourData.map(([key, value], index) => {
					return (
						<div key={key} className="opening__hours__item">
							<div>
								<span className="opening__hours__day">
									{key.charAt(0).toUpperCase() + key.slice(1)}
								</span>
								<span className="opening__hours__today">{checkToday(key)}</span>
							</div>
							<div>{diplayOpeningHours(value, index)}</div>
						</div>
					);
				})}
			</>
		</Card>
	);
};

export default OpeningHour;
