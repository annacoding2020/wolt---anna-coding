// import React from 'react';
import { render } from '@testing-library/react';
import { getAllByText } from '@testing-library/dom';

import OpeningHour from './OpeningHour';
import { OpeningHourState } from '@actionTypes/OpeningHourTypes';

describe('Unit test: Components: OpeningHour', () => {
	it('Render Opening Hour Components', () => {
		const { container, asFragment } = render(<OpeningHour openingHour={props} />);
		const dayClosed = getAllByText(container, 'Closed');
		expect(dayClosed).toHaveLength(2);
		const dayOpenUntilNextMorning = getAllByText(container, '10 AM - 1 AM');
		expect(dayOpenUntilNextMorning).toHaveLength(2);
		const dayOpenNomal = getAllByText(container, '10 AM - 6 PM');
		expect(dayOpenNomal).toHaveLength(1);
		const dayOpenMultipleTimes = getAllByText(container, '10 AM - 11:53 AM, 1:20 PM - 6 PM');
		expect(dayOpenMultipleTimes).toHaveLength(1);
		expect(asFragment()).toMatchSnapshot();
	});
});

const props: OpeningHourState = {
	isSaving: false,
	data: {
		monday: [],
		tuesday: [
			{ type: 'open', value: 36000 },
			{ type: 'close', value: 64800 },
			{ type: 'close', value: 42800 },
			{ type: 'open', value: 48000 },
		],
		wednesday: [],
		thursday: [
			{ type: 'open', value: 36000 },
			{ type: 'close', value: 64800 },
		],
		friday: [{ type: 'open', value: 36000 }],
		saturday: [
			{ type: 'close', value: 3600 },
			{ type: 'open', value: 36000 },
		],
		sunday: [
			{ type: 'close', value: 3600 },
			{ type: 'open', value: 43200 },
			{ type: 'close', value: 75600 },
		],
	},
};
