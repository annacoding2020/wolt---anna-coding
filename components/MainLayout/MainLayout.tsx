import React from 'react';
import { Layout } from 'antd';
import Link from 'next/link';

import './MainLayout.scss';

const { Header, Content, Footer, Sider } = Layout;

interface MainLayoutProps {
	children: React.ReactNode;
	siderChildren?: React.ReactNode;
}

const MainLayout: React.FunctionComponent<MainLayoutProps> = ({ children, siderChildren }) => {
	return (
		<Layout className="wolt">
			<Layout>
				<Header>
					<div className="logo">
						<Link href="/">
							<span>Wolt Opening Hours</span>
						</Link>
					</div>
				</Header>
				<Content>{children}</Content>
				<Footer>
					<summary>Anna Coding ©2020</summary>
				</Footer>
			</Layout>
			<Sider>{siderChildren}</Sider>
		</Layout>
	);
};

export default MainLayout;
