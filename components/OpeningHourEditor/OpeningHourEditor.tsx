import React, { useState, useEffect } from 'react';
import AceEditor from 'react-ace';
import { OpeningHourState, OpeningHourItem } from '@actionTypes/OpeningHourTypes';
import { Button } from 'antd';
import 'ace-builds/src-noconflict/mode-json';
import 'ace-builds/src-noconflict/theme-github';
import './OpeningHourEditor.scss';

interface OpeningHourEditorProps {
	openingHour: OpeningHourState;
	onSave: (data: OpeningHourItem[]) => void;
}

const OpeningHourEditor: React.FunctionComponent<OpeningHourEditorProps> = ({ openingHour, onSave }) => {
	const [error, setError] = useState<boolean>(false);
	const [jsonData, setJsonData] = useState<any>();

	useEffect(() => {
		setJsonData(openingHour.data)
	}, [openingHour]);

	const handleValidate = (error) => {		
		setError(!!error.length)
	};

	const handleChange = (value) => {
		try {
			setJsonData(JSON.parse(value))			
			setError(false)
		} catch (error) {
			setError(true)
		}
	};

	const handleSave  = () => {
		onSave(jsonData)
	}

	return (
		<div className="opening__hour__editor">
			<div className="opening__hour__editor_toolbox">
				<Button type="primary" block onClick={handleSave}>
					Save
				</Button>
			</div>
			<AceEditor
				mode="json"
				theme="github"
				readOnly={false}
				debounceChangePeriod={1000}
				value={JSON.stringify({ ...jsonData }, null, '\t')}
				height="2000px"
				width="100%"
				setOptions={{
					enableBasicAutocompletion: true,
					enableLiveAutocompletion: true,
					enableSnippets: true,
					showLineNumbers: true,
					tabSize: 2,
				}}
				editorProps={{ $blockScrolling: false }}
				onValidate={handleValidate}
				onChange={handleChange}
			/>
		</div>
	);
};

export default OpeningHourEditor;
