import {
	OpeningHourActionType,
	OpeningHour,
	SET_OPENING_HOUR,
	POST_OPENING_HOUR,
	IS_OPENING_HOUR_SAVING
} from '@actionTypes/OpeningHourTypes';

export function postOpeningHour(payload: OpeningHour): OpeningHourActionType {
	return {
		type: POST_OPENING_HOUR,
		payload,
	};
}

export function isOpeningHourSaving(isSaving: boolean): OpeningHourActionType {
	return {
		type: IS_OPENING_HOUR_SAVING,
		isSaving,
	};
}

export function setOpeningHour(payload: OpeningHour): OpeningHourActionType {
	return {
		type: SET_OPENING_HOUR,
		payload,
	};
}