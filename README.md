# Wolt Coding Task

## URL
[https://wolt.annacoding.com](https://wolt.annacoding.com/)

## Local
### Start
```
git clone ...
cd wolt
yarn install
yarn dev

```
Visit localhost:8008

### Test
Since time reason, only written 1 unit test.

```
yarn test
or
yarn test:u
```